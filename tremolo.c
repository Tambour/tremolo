/* includes */
#include "lv2.h"
#include "stddef.h"
#include "stdint.h"
#include "stdlib.h"
//#include "math.h"

/* class definitions */
typedef struct
{
/* data */
	float* audio_in_ptr;
	float* audio_out_ptr;
	float* level_ptr;
	float* rate_ptr;
	float* mode_ptr;
} MyTremolo;

/* internal core methods */
static LV2_Handle instantiate (const struct LV2_Descriptor *descriptor, double sample_rate, const
			char *bundle_path, const LV2_Feature *const *features)
{
	MyTremolo* t = (MyTremolo*) calloc (1, sizeof (MyTremolo));
	return t;
}

static void connect_port (LV2_Handle instance, uint32_t port, void *data_location)
/* handle provided by daw for every instance */
{
	MyTremolo* t = (MyTremolo*) instance;
	if (!t) return;
	
	switch (port)
	{
		case 0:
			t->audio_in_ptr = (float*) data_location;
			break;

		case 1:
			t->audio_out_ptr = (float*) data_location;
			break;

		case 2:
			t->level_ptr = (float*) data_location;
			break;

		case 3:
			t->rate_ptr = (float*) data_location;
			break;

		case 4:
			t->mode_ptr = (float*) data_location;
			break;

	default:
		break;
	}
}

static void activate (LV2_Handle instance)
{
	/* not required */
}
static void run (LV2_Handle instance, uint32_t sample_count)
{
	MyTremolo* t = (MyTremolo*) instance;
	if (!t) return;
	if ((!t->audio_in_ptr) || (!t->audio_out_ptr) || (!t->level_ptr) || 
			(!t->rate_ptr) || (!t->mode_ptr)) return;
	for (uint32_t i = 0 ; i < sample_count ; ++i)
	{
		t->audio_out_ptr[i] = t->audio_in_ptr[i] * *(t->level_ptr);
	}
}
static void deactivate (LV2_Handle instance)
{
	/* not required */
}
static void cleanup (LV2_Handle instance)
{
	MyTremolo* t = (MyTremolo*) instance;
	if (!t) return;
	free (t);
}
static const void*	extension_data (const char *uri)
{
	return NULL;
}

/* descriptor */
static LV2_Descriptor const descriptor =
{
	"https://gitlab.com/Tambour/tremolo",
	instantiate,
	connect_port,
	activate /* or NULL */,
	run,
	deactivate /* or NULL */,
	cleanup,
	extension_data /* or NULL */
};

/* interface */
 LV2_SYMBOL_EXPORT const LV2_Descriptor* 	lv2_descriptor (uint32_t index)
{
	if (index == 0) return &descriptor;
	else return NULL;
}
